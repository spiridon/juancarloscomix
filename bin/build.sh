#!/bin/bash

# Very crude approach, rewrite later
outdir=./public
[ -d $outdir ] || : ${ERROR:?not in root dir}
templatedir=./template
sitedir=./site
imagesdir=./images

process_template() {
  tit="$(echo $title | sed -e "s/%N%/$current/g")"
  cat $1 \
    | sed -e "s@%TITLE%@${tit}@" \
    | sed -e "s@%FIRST%@${first_path}@" \
    | sed -e "s@%PREVIOUS%@${prev_path}@" \
    | sed -e "s@%NEXT%@${next_path}@" \
    | sed -e "s@%LAST%@${last_path}@" \
    | sed -e "s@%ORD%@${current}@" \
    | sed -e "s@%IMGPATH%@${img_path}@" \
    | sed -e "s@%IMGALT%@${img_alt}@" \
    | sed -e "s@%IMGTIP%@${img_tip}@"
}

dump_pre() {
  process_template ${templatedir}/pre.html >${outdir}/$current_path
}

dump_insert() {
  process_template ${templatedir}/insert.html >>${outdir}/$current_path
}

dump_post() {
  process_template ${templatedir}/post.html >>${outdir}/$current_path
}

first_path='#'
prev_path='#'
next_path='2.html'
last_path='#'

cat ${sitedir}/comics.lst | while read line
do
  case $line in
    "[title"*)
      title="$(echo $line | cut -d: -f2 | tr -d ']')"
      echo "TITLE $title"
    ;;
    "[first"*)
      first=$(echo $line | cut -d: -f2 | tr -d ']')
      echo "|. $first"
    ;;
    "[last"*)
      last=$(echo $line | cut -d: -f2 | tr -d ']')
      last_path=${last}.html
      echo -n $last_path >.lastpath
      echo ".| $last ($last_path)"
    ;;
    "[end]"*)
      dump_post
      echo ":: Finishing"
      break
    ;;
    "["*)
      if [ $unfinished ]
      then
        dump_post
        prev=$current
        prev_path=$current_path
      fi

      current=$(echo $line | cut -d'[' -f2 | tr -d ']')
      current_path=${current}.html

      if [[ $current == $last ]]
      then
        last_path='#'
        next_path='#'
      else
        next_path="$(( $current + 1 )).html"
      fi

      dump_pre
      unfinished=1
      first_path=${first}.html

      echo ":: $current"
    ;;
    *)
      img_path="$(echo $line | cut -d@ -f1)"
      img_alt="$(echo $line | cut -d@ -f2)"
      img_tip="$(echo $line | cut -d@ -f3)"
      dump_insert
      echo "-> $current: $line"
    ;;
  esac
done

cp -r ${templatedir}/css $outdir
lastpath="$(< .lastpath)"
rm .lastpath
cd $outdir
rm index.html
ln -s $lastpath index.html
echo ":: Done."
